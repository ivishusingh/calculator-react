import React, { Component } from "react";

export class Calculator extends Component {
  state = {
    screen: "",
    result: "",
    screen2: "0",

    value: "",
    operator: ""
  };
  add = () => {
    this.setState({
      value: this.state.screen,
      screen: "",
      operator: "add"
    });
  };
  sub = () => {
    this.setState({
      value: this.state.screen,
      screen: "",
      operator: "sub"
    });
  };
  mul = () => {
    this.setState({
      value: this.state.screen,
      screen: "",
      operator: "mul"
    });
  };
  divide=()=>{
      this.setState({
          value:this.state.screen,
          screen:'',
          operator:'divide'
      })
  }
  clear = () => {
    this.setState({ screen: "" });
  };

  seven = () => {
    this.setState({ screen: this.state.screen.concat(7) });
  };
  eight = () => {
    this.setState({ screen: this.state.screen.concat(8) });
  };
  nine = () => {
    this.setState({ screen: this.state.screen.concat(9) });
  };
  four = () => {
    this.setState({ screen: this.state.screen.concat(4) });
  };
  five = () => {
    this.setState({ screen: this.state.screen.concat(5) });
  };
  six = () => {
    this.setState({ screen: this.state.screen.concat(6) });
  };
  one = () => {
    this.setState({ screen: this.state.screen.concat(1) });
  };
  two = () => {
    this.setState({ screen: this.state.screen.concat(2) });
  };
  three = () => {
    this.setState({ screen: this.state.screen.concat(3) });
  };
  zero = () => {
    this.setState({ screen: this.state.screen.concat(0) });
  };
  showResult = () => {
    switch (this.state.operator) {
      case "add": {
        let z = Number(this.state.value) + Number(this.state.screen);
        this.setState({ screen: z });
        break;
      }

      case "sub": {
        let a = Number(this.state.value) - Number(this.state.screen);
        this.setState({ screen: a });
        break;
      }
      case "mul": {
        let b = Number(this.state.value) * Number(this.state.screen);
        this.setState({ screen: b });
        break;
      }
      case "divide":{
        let b = (Number(this.state.value) / Number(this.state.screen)).toFixed(2);
      
        this.setState({ screen: b });
        console.log(b)
        break;  
      }
    }
  };
  render() {
    if (this.state.screen.length > 7) {
      alert("limit exceeded");
    }
    return (
      <div>
        <div className="calculator">
          <div className="calculator-screen">{this.state.screen} </div>
          <div
            onClick={this.clear}
            id="c"
            className="calculator-button calculator-function"
          >
            C
          </div>
          <div id="+/-" className="calculator-button calculator-function">
          N/a
          </div>
          <div id="%" className="calculator-button calculator-function">
          N/a
          </div>
          <div onClick={this.divide} id="/" className="calculator-button calculator-operator">
            ÷
          </div>
          <div
            onClick={this.seven}
            className="calculator-button calculator-number"
          >
            7
          </div>
          <div
            onClick={this.eight}
            className="calculator-button calculator-number"
          >
            8
          </div>
          <div
            onClick={this.nine}
            className="calculator-button calculator-number"
          >
            9
          </div>
          <div
            onClick={this.mul}
            className="calculator-button calculator-operator"
          >
            ×
          </div>
          <div
            onClick={this.four}
            className="calculator-button calculator-number"
          >
            4
          </div>
          <div
            onClick={this.five}
            className="calculator-button calculator-number"
          >
            5
          </div>
          <div
            onClick={this.six}
            className="calculator-button calculator-number"
          >
            6
          </div>
          <div
            onClick={this.sub}
            className="calculator-button calculator-operator"
          >
            -
          </div>
          <div
            onClick={this.one}
            className="calculator-button calculator-number"
          >
            1
          </div>
          <div
            onClick={this.two}
            className="calculator-button calculator-number"
          >
            2
          </div>
          <div
            onClick={this.three}
            className="calculator-button calculator-number"
          >
            3
          </div>
          <div
            onClick={this.add}
            className="calculator-button calculator-operator"
          >
            +
          </div>
          <div
            onClick={this.zero}
            className="calculator-button calculator-number calculator-zero"
          >
            0
          </div>
          <div className="calculator-button calculator-number">.</div>
          <div
            onClick={this.showResult}
            className="calculator-button calculator-operator"
          >
            =
          </div>
        </div>
      </div>
    );
  }
}

export default Calculator;
